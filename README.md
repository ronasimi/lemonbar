This is a status bar for i3wm, primarily written in bash.  You will need some packages to have this run correctly. On Arch, these are all available in the repos or the AUR, and I've noted the source for each below:

i3(-gaps) - obviously (repos/AUR)
ttf-material-design-icons-git - font for icons (AUR)
tamzen-font-git - font for bar text (AUR)
jq - parses output of i3-msg to get current container layout (splith/splitv/tabbed/stacked) (repos)  
xtitle - used to grab active window title (AUR)
pamixer - used to get current volume/mute status (repos)
networkmanager - (nmcli) is used to get current wifi and ethernet status (repos)
acpi - used to get current battery and AC adapter info (repos)
xbacklight - used to get/set backlight info (repos)
xdo - used to keep lemonbar below fullscreen windows
